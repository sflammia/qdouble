# Compute the fusion coefficients given a list of particles x
# where x is a vector of nonnegative integers of particle types
# The first input is the modular S matrix
# The vector x should be a list of particles of the following form.
# For example, if you wanted to fuse 3 copies of particle of type “5”,
# 2 copies of particles of type “7”, and 1 copy of particle “8”, you would
# input [5,5,5,7,7,8] for x.
# Something to do in the future would be to make a more efficient encoding
# so that you don’t have to enter the same particle multiple times, but 
# could instead write, e.g. [[5,3],[7,2],[8,1]]. 
# But I haven’t implemented that yet.
 
Fusion:=function(S,x)

local N,c,d,s,t,L,j;

N:=[];
L:=Size(x);

for c in [1..NumberOfParticles] do
  N[c]:=0;
for d in [1..NumberOfParticles] do
  s:=S[d];
  t:=1;
  for j in [1..L] do
    t:=t*s[x[j]];
  od;
  N[c]:=N[c]+t*ComplexConjugate(s[c])/(s[1])^(L-1);
od;
od;

return N;

end;
