### Calculate the modular T-Matrix ###

ModularTMatrix:=function(G)

local T,a,x,AX;

T:=[0]; # Initialize

for a in [1..Size(CC)] do   # Conjugacy Class
for x in [1..ChargesPerFlux[a]] do   # Charges in A
	# Calculate the current particle index
	AX:=Sum(ChargesPerFlux{[1..(a-1)]})+x;
	T[AX]:=(CCreps[a]^Chars[a][x])/Chars[a][x][1];
od;
od;

return T;
end;
