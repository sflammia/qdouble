# This outputs the particle dimensions, organized by flux sector
ParticleDimensions:=function(G)

local fluxsize,chargesize,dims,j;

fluxsize:=List(CC,Size);
chargesize:=List(Chars,x->TransposedMat(x)[1] );

dims:=[];
for j in [1..Size(fluxsize)] do
	dims[j]:=fluxsize[j]*chargesize[j];
od;

return dims;
end;
