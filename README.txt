This is a library of functions for calculating the particle spectrum,
fusion rules, S matrix, etc. in a quantum double discrete 
gauge theory. It’s incomplete, but I’m putting it up in the hope that
someone might get some use out of it or maybe improve it.

The main file is “QuantumDouble.g” which you can read in via

Read(“<path>/QuantumDouble.g”);

in GAP. That file loads some global variables and also defines
several functions. It has some additional documentation as well.