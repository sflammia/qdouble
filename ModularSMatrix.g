### Calculate the modular S-Matrix ###
ModularSMatrix:=function(G)

local S,a,b,x,y,i,j,AX,BY,CCelemsA,CCelemsB,char1,char2;

S:=NullMat(NumberOfParticles,NumberOfParticles); # Initialize

for a in [1..Size(CC)] do   # Conjugacy Class A
for b in [1..Size(CC)] do   # Conjugacy Class B
	# Generate the Class Elements via the Transversal
	CCelemsA:=List(Trans[a],z->CCreps[a]^z);
	CCelemsB:=List(Trans[b],z->CCreps[b]^z);
for x in [1..ChargesPerFlux[a]] do   # Charges in A
for y in [1..ChargesPerFlux[b]] do   # Charges in B
	# Calculate the current particle index
	AX:=Sum(ChargesPerFlux{[1..(a-1)]})+x;
	BY:=Sum(ChargesPerFlux{[1..(b-1)]})+y;
	# Now sum on commuting elements in A and B
	for i in [1..Size(CCelemsA)] do
	for j in [1..Size(CCelemsB)] do
		if CCelemsA[i]*CCelemsB[j]=CCelemsB[j]*CCelemsA[i] then
			char1:=((CCelemsB[j]^(Trans[a][i]^-1))^Chars[a][x]);
			char2:=((CCelemsA[i]^(Trans[b][j]^-1))^Chars[b][y]);
			S[AX][BY]:=S[AX][BY]+(char1*char2);
		fi;
	od;
	od;
	# Complex Conjugate
	S[AX][BY]:=ComplexConjugate(S[AX][BY]);
od;
od;
od;
od;

# Normalize
S:=S/Size(G);

return S;

end;
