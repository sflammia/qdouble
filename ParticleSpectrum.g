# Calculate the basic particles and properties of the theory.
ParticleSpectrum:=function(G);

CC:=ConjugacyClasses(G);
CCreps:=List(CC, Representative); # flux labels
CZer:=List(CCreps, x -> Centralizer(G,x) ); 

#
# Fix Trans so that it is guaranteed to work!!!!
#
Trans:=List(CZer, x -> RightTransversal(G,x)); # canonical coset elements
#
#

Chars:=List(CZer,Irr); # characters for the centralizer charges
ChargesPerFlux:=List(CZer,NrConjugacyClasses); 
NumberOfParticles:=Sum(ChargesPerFlux);

return NumberOfParticles;

end;
