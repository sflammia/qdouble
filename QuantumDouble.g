# This is a  library of functions for calculating the particle spectrum,
# fusion rules, and braid matrices in a quantum double discrete 
# gauge theory with anyonic quasiparticles.
# 
# To use it, first define a group G in GAP and then load this function
# using the command:
# 
# Read(“<path>/QuantumDouble.g”);
# 
# Steve Flammia, March 2006
#
#

# Initialize global variables
CC:=0;
CCreps:=0;
CZer:=0;
Trans:=0;
Chars:=0;
ChargesPerFlux:=0;
NumberOfParticles:=0;


Read("ParticleSpectrum.g");
# 
# ParticleSpectrum(G)
# 
# Calculates all of the above global variables for 
# the quantum double of G and outputs the total number
# of particles in the theory 

Read("ParticleDimensions.g");
# 
# ParticleDimensions(G)
# 
# Outputs the dimensions of the particles, organized
# by flux labels of charge sectors

Read("ModularSMatrix.g");
# 
# ModularSMatrix(G)
# 
# The modular S-matrix of the quantum double

Read("ModularTMatrix.g");
# 
# ModularTMatrix(G)
# 
# Returns a row vector which is the diagonal
# of the modular T-matrix

Read("Fusion.g");
# 
# Fusion(S,x)
# 
# Calculates the fusion rules for the
# list of particles x 


#####################################
#	NOT READY YET
#####################################
# Read("BraidMatrix.g");
# 
# BraidMatrix(G,a,b)
# 
# Calculates a braid matrix for particles a and b

